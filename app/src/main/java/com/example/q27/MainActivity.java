package com.example.q27;

import android.os.Bundle;

import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;

import androidx.core.view.WindowCompat;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.q27.databinding.ActivityMainBinding;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Button btn;
    EditText etext1, etext2, etext3, etext4, etext5;
    TextView text2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = findViewById(R.id.btn);
        etext1 = findViewById(R.id.etext1);
        etext2 = findViewById(R.id.etext2);
        etext3 = findViewById(R.id.etext3);
        etext4 = findViewById(R.id.etext4);
        etext5 = findViewById(R.id.etext5);
        text2 = findViewById(R.id.text2);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int sub1 = Integer.parseInt(etext1.getText().toString());
                int sub2 = Integer.parseInt(etext2.getText().toString());
                int sub3 = Integer.parseInt(etext3.getText().toString());
                int sub4 = Integer.parseInt(etext4.getText().toString());
                int sub5 = Integer.parseInt(etext5.getText().toString());

                int  total = sub1+sub2+sub3+sub4+sub5;
                int percentage = ((total)/500)*100;

                if (percentage>=70)
                {
                    text2.setText("The grade scored by the student is: A");
                }
                else if (percentage>=60)
                {
                    text2.setText("The grade scored by the student is: B");
                }
                else if (percentage>=50)
                {
                    text2.setText("The grade scored by the student is: C");
                }
                else if (percentage>=40)
                {
                    text2.setText("The grade scored by the student is: D");
                }
                else if (percentage>=30)
                {
                    text2.setText("The grade scored by the student is: E");
                }
                else if (percentage<=30)
                {
                    text2.setText("The grade scored by the student is: F");
            }
            }
        });

    }
}